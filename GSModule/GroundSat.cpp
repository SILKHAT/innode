/*
 * GroundSat.cpp  Version 1.2
 *
 * Copyright (c) 2013 Taygun Kekec
 *
 * Sponsored by InnovaThink
 */

/*
 * This program does ...
 */

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ncurses.h>
#include <math.h>
#include <pthread.h>
#include <time.h>
//Standart Networking
#include <sys/socket.h>
#include <netinet/in.h>

/*
 * LibRobotInno Includes
 */
//Joystick libraries
#include <linux/input.h>
#include <linux/joystick.h>
#include "../librobotinno/joystick/axbtnmap.h"
//Network Libraries
#include "../librobotinno/network/Package.h"
#include "../librobotinno/network/PackageConstants.h"
#include "../librobotinno/network/Client.h"
//Core Libraries
#include "../librobotinno/core/suShmLib.h"
// SystemConfig
#include "../librobotinno/SystemConfig.h"
// Utility Functions
#include "../librobotinno/core/Utility.h"
#include "../librobotinno/tiC2000.h"

/* Ps3 Controller macros */
#define LeftButton 7
#define RightButton 5
#define UpButton 4
#define downButton 6
#define selectButton 0
#define startButton 3
#define L1Button 10
#define R1Button 11
#define L2Button 8
#define R2Button 9
#define SquareButton 15
#define CircleButton 13
#define triangleButton 12
#define crossButton 14
//Axes
#define leftAnalogHoriAxis 0
#define leftAnalogVertAxis 1
#define rightAnalogHoriAxis 2
#define rightAnalogVertAxis 3

//Macros for access
#define thrustControl leftAnalogVertAxis
#define rollControl rightAnalogHoriAxis
#define pitchControl rightAnalogVertAxis
#define yawControl leftAnalogHoriAxis

char *axis_names[ABS_MAX + 1] = {
"X", "Y", "Z", "Rx", "Ry", "Rz", "Throttle", "Rudder",
"Wheel", "Gas", "Brake", "?", "?", "?", "?", "?",
"Hat0X", "Hat0Y", "Hat1X", "Hat1Y", "Hat2X", "Hat2Y", "Hat3X", "Hat3Y",
"?", "?", "?", "?", "?", "?", "?",
};

char *button_names[KEY_MAX - BTN_MISC + 1] = {
"Btn0", "Btn1", "Btn2", "Btn3", "Btn4", "Btn5", "Btn6", "Btn7", "Btn8", "Btn9", "?", "?", "?", "?", "?", "?",
"LeftBtn", "RightBtn", "MiddleBtn", "SideBtn", "ExtraBtn", "ForwardBtn", "BackBtn", "TaskBtn", "?", "?", "?", "?", "?", "?", "?", "?",
"Trigger", "ThumbBtn", "ThumbBtn2", "TopBtn", "TopBtn2", "PinkieBtn", "BaseBtn", "BaseBtn2", "BaseBtn3", "BaseBtn4", "BaseBtn5", "BaseBtn6", "BtnDead",
"BtnA", "BtnB", "BtnC", "BtnX", "BtnY", "BtnZ", "BtnTL", "BtnTR", "BtnTL2", "BtnTR2", "BtnSelect", "BtnStart", "BtnMode", "BtnThumbL", "BtnThumbR", "?",
"?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
"WheelBtn", "Gear up",
};

const char *stickDevice = "/dev/input/js0";
#define NAME_LENGTH 128
int *axis;
char *button;

//Joystick cmd
int thrustCmd;
int RollCmd;
int PitchCmd;
int YawCmd;

//MonitorApp variables
inline float deg2rad(float x) { return x * (180/M_PI); }
enum { SHOW_STATES, SHOW_LOGICS};

int cursesMain();
WINDOW *create_newwin(int height, int width, int starty, int startx);
void destroy_win(WINDOW *local_win);


//System Threads
pthread_t joystickThread;                            //
pthread_t tcpClientTransmitterThread;                //
pthread_t consoleMonitorThread;                      //
pthread_t groundTCPReceiverThread;				 	 //
pthread_t loggerThread;				 	 			//

void initThreads();
void *joystickThreadRun(void *param);
void *tcpClientTransmitterThreadRun(void *param);
void *consoleMonitorThreadRun(void *param);
void *groundTCPReceiverThreadRun(void *param);
void *loggerThreadRun(void *param);

short commandToSend = -1;

void initThreads()
{
        int  iret1, iret2, iret3, iret4, iret5;
        /* Create independent threads each of which will execute function */
        iret1 = pthread_create( &joystickThread, NULL, joystickThreadRun, (void*) NULL);   // For joystick
        iret2 = pthread_create( &tcpClientTransmitterThread, NULL, tcpClientTransmitterThreadRun, (void*)NULL );
        iret3 = pthread_create( &consoleMonitorThread, NULL, consoleMonitorThreadRun, (void*)NULL );
        iret4 = pthread_create( &groundTCPReceiverThread, NULL, groundTCPReceiverThreadRun, (void*)NULL );
        iret5 = pthread_create( &loggerThread, NULL, loggerThreadRun, (void*)NULL );
}

void *joystickThreadRun(void *param)
{

	int fd, i;
	unsigned char axes = 2;
	unsigned char buttons = 2;
	int version = 0x000800;
	char name[NAME_LENGTH] = "Unknown";
	uint16_t btnmap[BTNMAP_SIZE];
	uint8_t axmap[AXMAP_SIZE];
	int btnmapok = 1;

	if ((fd = open(stickDevice, O_RDONLY)) < 0) {
		printf("[%s]:Unable to open device", __func__);
		return NULL;
	}

	ioctl(fd, JSIOCGVERSION, &version);
	ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);
	ioctl(fd, JSIOCGNAME(NAME_LENGTH), name);

	getaxmap(fd, axmap);
	getbtnmap(fd, btnmap);

	printf("Driver version is %d.%d.%d.\n",version >> 16, (version >> 8) & 0xff, version & 0xff);

	/* Determine whether the button map is usable. */
	for (i = 0; btnmapok && i < buttons; i++) {
		if (btnmap[i] < BTN_MISC || btnmap[i] > KEY_MAX) {
			btnmapok = 0;
			break;
		}
	}
	if (!btnmapok) {
		/* btnmap out of range for names. Don't print any. */
		puts("jstest is not fully compatible with your kernel. Unable to retrieve button map!");
		//printf("Joystick (%s) has %d axes ", name, axes);
		//printf("and %d buttons.\n", buttons);
	} else {
/*		printf("Joystick (%s) has %d axes (", name, axes);
		for (i = 0; i < axes; i++)
			printf("%s%s", i > 0 ? ", " : "", axis_names[axmap[i]]);
		puts(")");

		printf("and %d buttons (", buttons);
		for (i = 0; i < buttons; i++) {
			printf("%s%s", i > 0 ? ", " : "", button_names[btnmap[i] - BTN_MISC]);
		}
		puts(").");*/
	}


	/*
	* Event interface, single line readout.
	*/
	struct js_event js;

	axis = static_cast <int*> (calloc(axes, sizeof(int)) );
	button = static_cast <char*> (calloc(buttons, sizeof(char)) );

	while (1) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			printf("\n[%s]:error reading", __func__);
			return NULL;
		}

		switch(js.type & ~JS_EVENT_INIT) {
		case JS_EVENT_BUTTON:
			button[js.number] = js.value;
			break;
		case JS_EVENT_AXIS:
			axis[js.number] = js.value;
			break;
		}

		/*printf("\r");

		if (axes) {
			printf("Axes: ");
			for (i = 0; i < axes; i++)
				printf("%2d:%6d ", i, axis[i]);
		}
		printf("\n ");
		if (buttons) {
			printf("Buttons: ");
			for (i = 0; i < buttons; i++)
				printf("%2d:[%d] ", i, button[i]);
		}*/

		thrustCmd = axis[thrustControl];
		RollCmd = axis[rollControl];
		PitchCmd = axis[pitchControl];
		YawCmd = axis[yawControl];
		//printf("T: %d  R : %d  P : %d  Y : %d \n", thrustCmd, RollCmd, PitchCmd, YawCmd);

		//fflush(stdout);
		usleep(5000);
	}

	return NULL;
}


void *loggerThreadRun(void *param)
{

	char *shm;
	//Bind to local shared mem
	if ( -1 == bindSharedMem(&shm ) ){
		printf("SharedMemoryBinding Failed: loggerThreadRun");
		exit(1);
	}

	static char FlightLogFileString[127];
	static char folderName[127];
	char timeStr[100];
	char todayDate[100];

	/* TO BE IMPLEMENTED
//Check for BlackBox folder's folder size
//If foldersize is greater than 10mb, dont start the autopilot and warn
//User to backup the Blackbox folder.
	 */

	//Create Log directories if NOT existing........
	strcpy(folderName, logPathRoot);
	DIR *dir = opendir(folderName);
	if ( dir == NULL ){
		mkdir(folderName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);	//Create logRoot directory
		strcat(folderName, flightLogPath);
		mkdir(folderName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);	//Create flightLogRoot directory
		strcpy(folderName, logPathRoot);
		strcat(folderName, algLogPath);
		mkdir(folderName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);	//Create algorithmLogRoot directory
	}

	char *datePtr;
	strcpy(FlightLogFileString, logPathRoot);	//Copy LogPathRoot
	strcat(FlightLogFileString, flightLogPath);	//Append FlightLogPath
	//datePtr = getDateString(); //Append date string to FlightLogFileString
	//strReplace(datePtr, " ", "_" , &todayDate[0]); //Replace whitespaces with _
	//strcat(FlightLogFileString, &todayDate[0]); //Append today's hour::min
	getTimeString(&timeStr[0]);
	strcat(FlightLogFileString, timeStr);
	printf("Recording to file %s \n", FlightLogFileString);
	strcat(FlightLogFileString, ".txt");	//Append .txt extension to FlightLogFileString

	FILE *logFile;

	//Get references from shared memory
	//Shared Memory Variables
    // Example access using pointer notation ( Variables will be updated, but using this is prone to errors

	//Logicals
    char *quadPidTest = (char*) getSM(shm, SM::QR::quadPidTest);
    char *systemFailure = (char*) getSM(shm, SM::systemFailure);
    char *platformType = (char*) getSM(shm, SM::platformType);
    char *sysFail = (char*) getSM(shm, SM::systemFailure);
    char *adcTest = (char*) getSM(shm, SM::adcTest);
    char *serial1Test = (char*) getSM(shm, SM::serial1Test);
    char *rfControlToggle = (char*) getSM(shm, SM::rfControlToggle);
    char *computerControlToggle = (char*) getSM(shm, SM::computerControlToggle);

    //States
    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);
    float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);
    float *pitchRate =  (float*) getSM(shm, SM::QR::pitchRate);
    float *yawRate =  (float*) getSM(shm, SM::QR::yawRate);
    float *xPos =  (float*) getSM(shm, SM::xPos);
    float *yPos =  (float*) getSM(shm, SM::yPos);
    float *zPos =  (float*) getSM(shm, SM::zPos);

    int *md1 = (int*) getSM(shm, SM::QR::motorDuty1);
    int *md2 = (int*) getSM(shm, SM::QR::motorDuty2);
    int *md3 = (int*) getSM(shm, SM::QR::motorDuty3);
    int *md4 = (int*) getSM(shm, SM::QR::motorDuty4);
    int *u1 = (int*) getSM(shm, SM::QR::U1);
    int *u2 = (int*) getSM(shm, SM::QR::U2);
    int *u3 = (int*) getSM(shm, SM::QR::U3);
    int *u4 = (int*) getSM(shm, SM::QR::U4);
    int *rf1 = (int*) getSM(shm, SM::RFDuty1);
    int *rf2 = (int*) getSM(shm, SM::RFDuty2);
    int *rf3 = (int*) getSM(shm, SM::RFDuty3);
    int *rf4 = (int*) getSM(shm, SM::RFDuty4);

  //  float *desAlt = (float*) getSM( shm, SM::QR::desAltitude);


	while( 1 ){

		logFile = fopen(FlightLogFileString, "a+");
		//Get timeString
		datePtr = getDateString();

		//Append shared memory to logfile
		fprintf(logFile, "ROLL: %.5f PITCH: %.5f YAW: %.5f SONAR1: %.5f MEDIANFILTERED: %.5f MEDIANDIFF: %.5f MEDIANINT: %.5f RAWSONAR: %.5f ", *roll, *pitch, *yaw, 1, 1, 1, 1, 1);
		fprintf(logFile, "ROLLRATE: %.5f PITCHRATE: %.5f ", *rollRate, *pitchRate);
		fprintf(logFile, "U1: %.5f U2: %.5f U3: %.5f U4: %.5f ", *u1, *u2, *u3, *u4);
		fprintf(logFile, "MDuty1: %d MDuty2: %d MDuty3: %d MDuty4: %d ", *md1, *md2, *md3, *md4 );
		fprintf(logFile, "ChDuty1: %d ChDuty2: %d ChDuty3: %d ChDuty4: %d ", *rf1, *rf2, *rf3, *rf4);
		fprintf(logFile, "DesiredAltitude: %.2f HoverMode: %f flightAllow: %f ", 1.0, 1.0, 1.0);
		fprintf(logFile, " TIME:%s\n", datePtr);
		//Append systemStatus to logfile

		//write necessary variables....

		fclose(logFile);
		usleep(loggerSleepPeriod * 1000);
	}

}

void *tcpClientTransmitterThreadRun(void *param)
{
	int readCode = -1;
	int MAXSIZE = 1024;

	printf("Started tcpClientThreadRun \n");
	//Dummy package
    Package package;

	Client client;

	//TCP Networking Variables
    struct sockaddr_in server_info;
    struct hostent *he;
    int socket_fd,num;
    char buffer[MAXSIZE];
    char recvBuff[64];
    if ((he = gethostbyname("10.42.0.81"))==NULL) {
        fprintf(stderr, "Cannot get host name\n");
        exit(1);
    }
    startConnection:

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0))== -1) {
        fprintf(stderr, "Socket Failure!!\n");
        exit(1);
    }
    memset(&server_info, 0, sizeof(server_info));
    server_info.sin_family = AF_INET;
    server_info.sin_port = htons(3495);
    server_info.sin_addr = *((struct in_addr *)he->h_addr);
    readCode = -1;
    while( readCode == -1){
        readCode = connect(socket_fd, (struct sockaddr *)&server_info, sizeof(struct sockaddr));
        perror("connect");
        usleep(1000 * 500);
    }

	while(1 == 1){
		memset(buffer, 0, MAXSIZE);

		if (commandToSend != -1){
			package.initialize(PCK_COMMAND);
			package.value[0] = commandToSend;
			commandToSend = -1; //Make it clear that we sent the package

		}
		else{
			//Nothing has been declared send joystick package periodically
			package.initialize(PCK_DATA_JOYSTICK);
			package.value[0] = ((char*)(&thrustCmd))[0];
			package.value[1] = ((char*)(&thrustCmd))[1];
			package.value[2] = ((char*)(&thrustCmd))[2];
			package.value[3] = ((char*)(&thrustCmd))[3];

			package.value[4] = ((char*)(&YawCmd))[0];
			package.value[5] = ((char*)(&YawCmd))[1];
			package.value[6] = ((char*)(&YawCmd))[2];
			package.value[7] = ((char*)(&YawCmd))[3];

			package.value[8] = ((char*)(&RollCmd))[0];
			package.value[9] = ((char*)(&RollCmd))[1];
			package.value[10] = ((char*)(&RollCmd))[2];
			package.value[11] = ((char*)(&RollCmd))[3];

			package.value[12] = ((char*)(&PitchCmd))[0];
			package.value[13] = ((char*)(&PitchCmd))[1];
			package.value[14] = ((char*)(&PitchCmd))[2];
			package.value[15] = ((char*)(&PitchCmd))[3];
		}

    	usleep(1000 * 500);
    	//*RF1 = *RF1 + 1;

    	package.length = sizeof(package);
	    package.serialize(buffer);

        //fgets(buffer,MAXSIZE-1,stdin);
	    printf("BufferLength: %d \n", sizeof(package));


        if ((send(socket_fd,buffer, sizeof(package) , 0) == -1)) {
                fprintf(stderr, "Failure Sending Message\n");
                close(socket_fd);
                exit(1);
        }
        else {

  				printf("Sending Message:\n");

                printf("[%d]", buffer[0]);
                short *ln = (short *) &buffer[1];
                printf("[%d]", *ln);
                for(int i = 3; i < 6; i++) { printf("[%d]", buffer[i]); }
                printf("\n");

                num = recv(socket_fd, recvBuff, sizeof(recvBuff),0);
                if ( num <= 0 )
                {
                        printf("Either Connection Closed or Error\n");
                        //Break from the While
                        goto startConnection;
                }

                //printf("Client:Message Received From Server -  %s\n",recvBuff);
           }
	}
    close(socket_fd);


}

void *consoleMonitorThreadRun(void *param)
{
	char *shm;
	int currTab = 0, maxTab = 3;	// 3 Different tabs maintained. 1-Platform states 2-Package Transmissions 3- Log
	char showLogics = false;

	int i=0, ch;
	WINDOW *my_win;
	int startx, starty, width, height;

	//Bind to local shared mem
	if ( -1 == bindSharedMem(&shm ) ){
		printf("SharedMemoryBinding Failed: cursesMain");
		exit(1);
	}

	//Screen Initialization
	initscr();					/* Start curses mode 		  */
	raw();						/* Line buffering disabled	*/
	keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/
	echo();			/* Don't echo() while we do getch */

	height = 15;
	width = 70;
	starty = 0;	/* Calculating for a center placement */
	startx = 0;	/* of the window		*/


	if(has_colors() == FALSE)
	{	endwin();
		printf("Your terminal does not support color\n");
		exit(1);
	}

	start_color();			/* Start color 			*/
	init_color(COLOR_BLUE, 200, 200, 1000);
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);

	refresh();
	timeout(33);	// Update rate is 30hz
	my_win = create_newwin(height, width, starty, startx);

	//Shared Memory Variables
    // Example access using pointer notation ( Variables will be updated, but using this is prone to errors

	//Logicals
    char *quadPidTest = (char*) getSM(shm, SM::QR::quadPidTest);
    char *systemFailure = (char*) getSM(shm, SM::systemFailure);
    char *platformType = (char*) getSM(shm, SM::platformType);
    char *sysFail = (char*) getSM(shm, SM::systemFailure);
    char *adcTest = (char*) getSM(shm, SM::adcTest);
    char *serial1Test = (char*) getSM(shm, SM::serial1Test);
    char *rfControlToggle = (char*) getSM(shm, SM::rfControlToggle);
    char *computerControlToggle = (char*) getSM(shm, SM::computerControlToggle);

    //States
    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);
    float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);
    float *pitchRate =  (float*) getSM(shm, SM::QR::pitchRate);
    float *yawRate =  (float*) getSM(shm, SM::QR::yawRate);
    float *xPos =  (float*) getSM(shm, SM::xPos);
    float *yPos =  (float*) getSM(shm, SM::yPos);
    float *zPos =  (float*) getSM(shm, SM::zPos);

    int *md1 = (int*) getSM(shm, SM::QR::motorDuty1);
    int *md2 = (int*) getSM(shm, SM::QR::motorDuty2);
    int *md3 = (int*) getSM(shm, SM::QR::motorDuty3);
    int *md4 = (int*) getSM(shm, SM::QR::motorDuty4);
    int *u1 = (int*) getSM(shm, SM::QR::U1);
    int *u2 = (int*) getSM(shm, SM::QR::U2);
    int *u3 = (int*) getSM(shm, SM::QR::U3);
    int *u4 = (int*) getSM(shm, SM::QR::U4);
    int *rf1 = (int*) getSM(shm, SM::RFDuty1);
    int *rf2 = (int*) getSM(shm, SM::RFDuty2);
    int *rf3 = (int*) getSM(shm, SM::RFDuty3);
    int *rf4 = (int*) getSM(shm, SM::RFDuty4);

	while((ch = getch()) != 'Q' )
	{

		clear();

		//Print program header
		move(0, 0);
		attron(COLOR_PAIR(1));
		attron(A_BOLD);
		printw("%s %s (C) 2012-2013 - %s ",consoleName, versionName, webPage);
		attroff(A_BOLD);
		attroff(COLOR_PAIR(1));

		//Print command line
		move( (height-1), 0);
		attron(COLOR_PAIR(1));
		attron(A_BOLD);
		printw("Platform:%s  Hulooogh...   ","quadrotor" );
		attroff(A_BOLD);
		attroff(COLOR_PAIR(1));

		i++;
		switch(currTab){
		case 0:
			//Print Here what you will print...
			attron(COLOR_PAIR(2));
			//Show Platform States or Logical Controls
			if ( !showLogics){
				move(1, 0); printw("Roll:  %5.2f", deg2rad(*roll) );
				move(1, 15);printw("RollRate:   %5.2f", deg2rad(*rollRate) );
				move(1, 33);printw("xPos:   %5.2f", (*xPos) );
				move(2, 0); printw("Pitch: %5.2f", deg2rad(*pitch) );
				move(2, 15);printw("PitchRate:  %5.2f", deg2rad(*pitchRate) );
				move(2, 33);printw("yPos:   %5.2f", (*yPos) );
				move(3, 0); printw("Yaw:   %5.2f", deg2rad(*yaw) );
				move(3, 15);printw("YawRate:    %5.2f", deg2rad(*yawRate) );
				move(3, 33);printw("zPos:   %5.2f", (*zPos) );
				move(4, 0); printw("U1:    %5d", *u1 );
				move(4, 15);printw("M1:    %5d", *md1 );
				move(4, 33);printw("RF1:    %5d", *rf1 );
				move(5, 0); printw("U2:    %5d", *u2 );
				move(5, 15);printw("M2:    %5d", *md2 );
				move(5, 33);printw("RF2:    %5d", *rf2 );
				move(6, 0); printw("U3:    %5d", *u3 );
				move(6, 15);printw("M3:    %5d", *md3 );
				move(6, 33);printw("RF3:    %5d", *rf3 );
				move(7, 0); printw("U4:    %5d", *u4 );
				move(7, 15);printw("M4:    %5d", *md4 );
				move(7, 33);printw("RF4:    %5d", *rf4 );

				move(12, 0); printw("curr:   %d", i);
				move(13, 0); printw("curr2:   %d", ch );


			}
			else{
				move(1, 0); printw("quadPidTest    : %s", (*quadPidTest) ? "On" : "Off");
				move(2, 0); printw("systemFailure  : %d", *systemFailure);
				move(3, 0); printw("platformType   : %d", *platformType);
				move(4, 0); printw("sysFail        : %d", *sysFail);
				move(5, 0); printw("adcTest        : %s", (*adcTest) ? "On" : "Off");
				move(6, 0); printw("serial1Test    : %s", (*serial1Test) ? "On" : "Off");
				move(7, 0); printw("rfControlToggle: %s", (*rfControlToggle) ? "On" : "Off");
				move(8, 0); printw("compCntrlToggle: %s", (*computerControlToggle) ? "On" : "Off");

			}

			//Write help
			move(1, 48);printw("==KEYS===" );
			move(2, 48);printw("t: States" );
			move(3, 48);printw("y: Logics" );
			move(4, 48);printw("0: Close " );

			attron(COLOR_PAIR(2));

			refresh();
			move( (height), 0);	// Wait for user input
			break;
		case 1: // Second tab

			move(1, 0); printw("CommSendTab");
			move(2, 0); printw("1. Initialize ESC");
			move(3, 0); printw("2. Allow Flight System to Run");
			move(4, 0); printw("3. Dİsable Flight System (Stop Motors)");
			move(5, 0); printw("4. Adjust Gains(ROLL)");
			move(6, 0); printw("5. Adjust Gains(PITCH)");
			move(7, 0); printw("6. Adjust Gains(YAW)");
			move(8, 0); printw("7. IMU Bias Zeroize");
			refresh();
			break;
		case 2: // Third tab
			move(1, 0); printw("DebugTab");
			refresh();
			break;
		}

		switch(ch){
			case '0':
					//Terminate the program
					endwin();			/* End curses mode		  */
					return 0;
					break;
			case 27:
				ch = getch();
				if ( ch == ERR){
					break; 			// ALT + ESC case
				}
				else if ( ch == '1'){	// ALT + 1
					//
					currTab > 0 ? currTab-- : currTab;	// go another tab
				}
				else if ( ch == '2'){	// ALT + 1
					//
					currTab < maxTab - 1 ? currTab++ : currTab;	// go to another tab
				}
				else{

				}
				break;
			case KEY_LEFT:
				//
				break;
			case KEY_RIGHT:
				//
				break;
			case 't':
				showLogics = SHOW_STATES; //If user presses up, show states
				break;
			case 'y':
				showLogics = SHOW_LOGICS; //If user presses Down, show logical control
				break;
			case '1':
				if ( currTab == 1 ){
					move(13, 0); printw("SENT ESC INIT PACKAGE.");
					//Declare TCPClientTransmitter to generate a command package
					commandToSend = TI_REMOTE_ESC_INIT;
					refresh();
					usleep(1000 * 500);
				}
				break;
			case '2':
				if ( currTab == 1 ){
					move(13, 0); printw("SENT FLIGHT ALLOW PACKAGE.");
					//Declare TCPClientTransmitter to generate a command package
					commandToSend = TI_REMOTE_ALLOW_FLIGHT;
					refresh();
					usleep(1000 * 500);
				}
				break;
			case '7':
				if ( currTab == 1 ){
					move(13, 0); printw("SENT IMU BIAS PACKAGE.");
					//Declare TCPClientTransmitter to generate a command package
					commandToSend = TI_REMOTE_IMU_BIAS_SET;
					refresh();
					usleep(1000 * 500);
				}
				break;
		}

	}

	endwin();			/* End curses mode		  */
	return 0;

}

void *groundTCPReceiverThreadRun(void *param)
{

	printf("[%s]:TCP server started\n",__func__);

	//Bind to shared mem
	char *shm;
	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

    //Shared Memory Snappings
    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);
    float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);
    float *pitchRate =  (float*) getSM(shm, SM::QR::pitchRate);
    float *yawRate =  (float*) getSM(shm, SM::QR::yawRate);
    float *xPos =  (float*) getSM(shm, SM::xPos);
    float *yPos =  (float*) getSM(shm, SM::yPos);
    float *zPos =  (float*) getSM(shm, SM::zPos);

    int *md1 = (int*) getSM(shm, SM::QR::motorDuty1);
    int *md2 = (int*) getSM(shm, SM::QR::motorDuty2);
    int *md3 = (int*) getSM(shm, SM::QR::motorDuty3);
    int *md4 = (int*) getSM(shm, SM::QR::motorDuty4);
    int *u1 = (int*) getSM(shm, SM::QR::U1);
    int *u2 = (int*) getSM(shm, SM::QR::U2);
    int *u3 = (int*) getSM(shm, SM::QR::U3);
    int *u4 = (int*) getSM(shm, SM::QR::U4);
    int *rf1 = (int*) getSM(shm, SM::RFDuty1);
    int *rf2 = (int*) getSM(shm, SM::RFDuty2);
    int *rf3 = (int*) getSM(shm, SM::RFDuty3);
    int *rf4 = (int*) getSM(shm, SM::RFDuty4);


    //TCP Networking
    struct sockaddr_in server;
    struct sockaddr_in dest;
    int status,socket_fd, client_fd,num;
    socklen_t size;

    char buffer[1024];
    char gotMes[64] = {'O','K'};
    char *buff;
    int yes =1;

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0))== -1) {
        fprintf(stderr, "Socket failure!!\n");
        exit(1);
    }

    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }
    memset(&server, 0, sizeof(server));
    memset(&dest,0,sizeof(dest));
    server.sin_family = AF_INET;
    server.sin_port = htons(3490);
    server.sin_addr.s_addr = INADDR_ANY;
    if ((bind(socket_fd, (struct sockaddr *)&server, sizeof(struct sockaddr )))== -1)    { //sizeof(struct sockaddr)
        fprintf(stderr, "Binding Failure\n");
        exit(1);
    }

    if ((listen(socket_fd, 10))== -1){
        fprintf(stderr, "Listening Failure\n");
        exit(1);
    }


    Package package;

    while(1) {
anotherclient:
        size = sizeof(struct sockaddr_in);

        if ((client_fd = accept(socket_fd, (struct sockaddr *)&dest, &size))==-1 ) {
            perror("accept");
            exit(1);
        }
        printf("Server got connection from client %s\n", inet_ntoa(dest.sin_addr));

        while(1) {
                if ((num = recv(client_fd, buffer, sizeof(package),0))== -1) {
                        perror("recv");
                        goto anotherclient;
                }
                else if (num == 0) {
                        printf("Connection closed\n");
                        //So I can now wait for another client
                        break;
                }

                printf("num %d \n", num);
                buffer[num] = '\0';

                printf("[%d]", buffer[0]);
                short *ln = (short *) &buffer[1];
                printf("[%d]", *ln);
                for(int i = 3; i < 6; i++) { printf("[%d]", buffer[i]); }
                printf("\n");

                //parse package
                package.deserialize(buffer);

            	printf("packlen: %d \n", package.length);
                if ( package.tag == PCK_DATA_SHM){
                	//Copy content to SHM
            		*rf1 = char_to_int( &package.value[0]);
            		*rf2 = char_to_int(&package.value[4]);
            		*rf3 = char_to_int(&package.value[8]);
            		*rf4 = char_to_int(&package.value[12]);
            		*md1 = char_to_int( &package.value[16]);
            		*md2 = char_to_int(&package.value[20]);
            		*md3 = char_to_int(&package.value[24]);
            		*md4 = char_to_int(&package.value[28]);
            		*roll = char_to_float( &package.value[32]);
            		*pitch = char_to_float(&package.value[36]);
            		*yaw = char_to_float(&package.value[40]);
            		*u1 = char_to_float( &package.value[44]);
            		*u2 = char_to_float(&package.value[48]);
            		*u3 = char_to_float(&package.value[52]);
            		*u4 = char_to_float(&package.value[56]);


                }

                if ((send(client_fd,gotMes, sizeof(gotMes),0))== -1)
                {
                     fprintf(stderr, "Failure Sending Message\n");
                     close(client_fd);
                     break;
                }


        } //End of Inner While...
        //Close Connection Socket
        close(client_fd);
    } //Outer While

    close(socket_fd);

	printf("[%s]:TCP server went down.\n",__func__);

}

WINDOW *create_newwin(int height, int width, int starty, int startx)
{	WINDOW *local_win;

	local_win = newwin(height, width, starty, startx);
	//box(local_win, 0 , 0);//  0, 0 gives default characters for the vertical and horizontal lines
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

void destroy_win(WINDOW *local_win)
{
	/* box(local_win, ' ', ' '); : This won't produce the desired
	 * result of erasing the window. It will leave it's four corners
	 * and so an ugly remnant of window.
	 */
	wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	/* The parameters taken are
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window
	 * 3. rs: character to be used for the right side of the window
	 * 4. ts: character to be used for the top side of the window
	 * 5. bs: character to be used for the bottom side of the window
	 * 6. tl: character to be used for the top left corner of the window
	 * 7. tr: character to be used for the top right corner of the window
	 * 8. bl: character to be used for the bottom left corner of the window
	 * 9. br: character to be used for the bottom right corner of the window
	 */
	wrefresh(local_win);
	delwin(local_win);
}

int main (int argc, char **argv)
{
	initThreads();
	while(1 == 1){
		usleep(10000);
	}

}

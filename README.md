SuPilot
=================
Project Author : Taygun Kekec

GNU LESSER GENERAL PUBLIC LICENSE

                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
 

This project is about extendible operating software of aerial and mobile robotic platforms. It aims to cover several robotic platforms such as quadrotors, mobile robots and underwater vehicles. Current project is mainly grouped into two modules: ground station module and core module which is to be run on embedded platform.

Current Project Modules:

GSModule - Ground Station Module to run on operator's computer
CoreModule - Embedded Application to run on device (Rasberry, Gumstix etc)
librobotinno - Various common code covering network, hardware, shared memory code stacks.

Build Steps:

cd librobotinno
make 
cd ../GSModule
make
cd ../CoreModule
make
cd ..

or run the file ./run

then you can run GroundStation by:

./GSModule/pairDevices   # For pairing PS3 joystick with GS computer
./GSModule/GroundSat

you can start core module by:
./CoreModule/coreModule1.0

NOTE: Currently we do not support dynamic IPs, you need to change TCP/IP connect lines with corresponding ground station and gumstix IPs.

/*
 ============================================================================
 Name        : su_autopilot.h
 Author      : Taygun Kekec
 Version     : 1.0
 Date		 : March 2013
 Copyright   : GNU
 Description :
			   This file covers entry point of SuPilot application
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>			//Utility Library
#include <pthread.h>		//Posix Thread Library
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>		//Unix Interprocess CommunicationLibrary
#include <sys/shm.h>		//Unix Shared Memory Library
#include <unistd.h>			//Parsing functionality
#include <ctype.h>

#include "../librobotinno/SystemConfig.h"
#include "SystemThreads.h"
#include "../librobotinno/core/Utility.h"
#include "../librobotinno/core/suShmLib.h"

/*System Thread Declarations */
pthread_t adcThread;				// copying ADC input to SharedMemory
pthread_t serial1Thread;			// copying Serial1 input to SharedMemory
pthread_t serial2Thread;			// copying Serial2 input to SharedMemory
pthread_t loggerThread;				// reading shared memory and writing states to logfile
pthread_t sysShmTransmitThread;		// transmitting SystemStatus to remote	(MAVLINK)
pthread_t tcpServerThread;			// handles incoming connections
pthread_t cameraThread;				// transmitting frames to the network
pthread_t quadPidThread;			// pid control for quadrotor
pthread_t quadMonitorThread;		// checks for failures and system states periodically

void print_help();
/* System Function Declarations */
void initSharedMemory();			// Initializes shared memory and its content
void initThreads();					// Initializes and starts threads
void initSystem();					// Initializes system specific data

void main_loop();					// Main program loop
void checkSensorFailure();			// Periodically checks sensorFailures
void checkSystemFailure();			// Periodically checks systemFailures

/*Shared Memory Access */
char *shm;							// Shared Memory instance

void print_help()
{
	printf("Usage: ./suPilot [options] [values]\n");
	printf("\n");
	printf("Valid Options\n");
	printf("-t Test Mode , 			values : adc, serial1, quadPid\n");
	printf("-p Platform Selection , values : quadrotor, mobile\n");

}
void main_loop()
{
	int i = 0;
	// Comment out all printfs.
	for(;;){
		usleep(2500000);
		printf("Cycle: %d \n" ,i++);
	}
}

void initThreads()
{
	int  iret1, iret2, iret3, iret4, iret5, iret6, iret7, iret8;
	/* Create independent threads each of which will execute function */
	iret1 = pthread_create( &adcThread, NULL, adcThreadRun, (void*) shm);	// For Serial IMU
	//iret2 = pthread_create( &serial1Thread, NULL, serial1ThreadRun, (void*) shm);	/* Serial2 is TI Comm for now */
	iret3 = pthread_create( &serial2Thread, NULL, serial2ThreadRun, (void*) shm);
	//iret4 = pthread_create( &loggerThread, NULL, loggerThreadRun, (void*) shm);
	//iret7 = pthread_create( &cameraThread, NULL, cameraThreadRun, (void*) shm);
	//iret7 = pthread_create( &quadPidThread, NULL, quadPidThreadRun, (void*) shm);
	iret8 = pthread_create( &quadMonitorThread, NULL, quadMonitorThreadRun, (void*) shm);
	iret5 = pthread_create( &sysShmTransmitThread, NULL, sysShmTransmitThreadRun, (void*) shm);
	iret6 = pthread_create( &tcpServerThread, NULL, tcpServerThreadRun, (void*) shm);
}

void initSharedMemory()
{

	if ( -1 == allocateSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed: adcThreadRun",__func__);
	}

	/* Initialize shared memory with SHARED_MEMORY_INIT_VAL to let client error check. */
	memset (shm, SHARED_MEMORY_INIT_VAL ,SHARED_MEMORY_SIZE);

	//Initialize IMU data
	writeToSM( shm, SM::QR::roll , 6.66f);
	writeToSM( shm, SM::QR::pitch , 0.05f);
	writeToSM( shm, SM::QR::yaw , 0.06f);
	//Initialize IMUrate data
	writeToSM( shm, SM::QR::rollRate , 0.01f);
	writeToSM( shm, SM::QR::pitchRate , 0.02f);
	writeToSM( shm, SM::QR::yawRate , 0.03f);
	//Initialize Virtual Control data
	writeToSM( shm, SM::QR::U1 , 1);
	writeToSM( shm, SM::QR::U2 , 2);
	writeToSM( shm, SM::QR::U3 , 3);
	writeToSM( shm, SM::QR::U4 , 4);
	//Initialize Motor Duties
	writeToSM( shm, SM::QR::motorDuty1 , 5);
	writeToSM( shm, SM::QR::motorDuty2 , 6);
	writeToSM( shm, SM::QR::motorDuty3 , 7);
	writeToSM( shm, SM::QR::motorDuty4 , 8);
	//Initialize RF signals
	writeToSM( shm, SM::RFDuty1 , 9);
	writeToSM( shm, SM::RFDuty2, 10);
	writeToSM( shm, SM::RFDuty3, 11);
	writeToSM( shm, SM::RFDuty4, 12);

	//Initialize System States
	writeToSM( shm, SM::systemFailure , 0);
	writeToSM( shm, SM::platformType , DEFS::PLATFORM::QUADROTOR);	// Use quadrotor as primary platform
	writeToSM( shm, SM::sysStatus , DEFS::STATES::RUNNING);			// Use quadrotor as primary platform

	//Debugging
	writeToSM( shm, SM::adcTest , 0);
	writeToSM( shm, SM::serial1Test , 0);
	writeToSM( shm, SM::serial2Test , 0);


}

void initSystem()
{

}
int main(int argc, char *argv[])
{
	//Initialize Shared Memory
	initSharedMemory();

	//Parse Program Arguments with Getopt
    int aflag = 0, bflag = 0, tflag = 0, pflag = 0;
    char *tvalue = NULL;
    char *pvalue = NULL;
    int index;
    int c;

    opterr = 0;

    while ((c = getopt(argc, argv, "abp:t:")) != -1){
      switch (c){
        case 'a':
        	aflag = 1;
        	break;
        case 'b':
        	bflag = 1;
        	break;
        case 'p':
        	pvalue = optarg;
        	pflag = 1;
        	break;
        case 't':
        	tvalue = optarg;
        	tflag = 1;
        	break;
        case '?':
		    if (optopt == 't'){
			  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		    }
		    else if (optopt == 'p'){
		  	  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		    }
		    else if (isprint (optopt))
			  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		    else{
			  fprintf (stderr,
					 "Unknown option character `\\x%x'.\n",
					 optopt);
		    }
		    print_help();
		    return 1;
        default:  return 0;
      }
    }

    //printf ("pflag = %d, tflag = %d, tvalue = %s\n", pflag, tflag, tvalue);

    if (pflag){
    	//Platform selection
    	if ( strcmp(pvalue,"quadrotor") == 0 ){
    		printf("[%s]:Platform selected as quadrotor\n", __func__);
    		//To Do: do all quad init here,
    		//if it exceeds more than 10 line, make a new function
    		writeToSM(shm, SM::platformType, (char) DEFS::PLATFORM::QUADROTOR);
    	}
    	else if ( strcmp(pvalue,"mobile") == 0 ){
    		printf("[%s]:Platform selected as mobile robot\n", __func__);
    		//To Do: do all mobile init here,
    		//if it exceeds more than 10 line, make a new function
    		writeToSM(shm, SM::platformType, (char) DEFS::PLATFORM::MOBILE_ROBOT);
    	}
    }
    else{
    	printf("[%s]:Platform is not given as an option. Selecting quadrotor by default\n",__func__);
		//To Do: do all quad init here
		//if it exceeds more than 10 line, make a new function
		writeToSM(shm, SM::platformType, (char) DEFS::PLATFORM::QUADROTOR);
    }

    if (tflag){
    	if ( strcmp(tvalue,"adc") == 0 ){
    		// Open ADC Testing
			printf("[%s]:Test mode activated\n",__func__);
			writeToSM(shm, SM::adcTest, (char)1);
			int iret1 = pthread_create( &adcThread, NULL, adcThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("ADC Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"serial1") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			writeToSM(shm, SM::serial1Test, (char)1);
			int iret2 = pthread_create( &serial1Thread, NULL, serial1ThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("Serial1 Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"serial2") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			writeToSM(shm, SM::serial2Test, (char)1);
			int iret2 = pthread_create( &serial2Thread, NULL, serial2ThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("Serial2 Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"quadPid") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			writeToSM(shm, SM::QR::quadPidTest, (char)1);
			int iret1 = pthread_create( &serial1Thread, NULL, serial1ThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			int iret2 = pthread_create( &quadPidThread, NULL, quadPidThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("quadPid Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"tcpServer") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			int iret1 = pthread_create( &tcpServerThread, NULL, tcpServerThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("tcpServer Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"transmitServer") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			int iret1 = pthread_create( &sysShmTransmitThread, NULL, sysShmTransmitThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("sysShmTransmitThread Poll.\n");
			}
    	}
    	else if ( strcmp(tvalue,"receiveServer") == 0 ){
    		// Open Serial1 Testing
			printf("[%s]:Test mode activated\n",__func__);
			int iret1 = pthread_create( &tcpServerThread, NULL, tcpServerThreadRun, (void*) shm);//This is Test Mode. Given argument will test ADC thread.
			for(;;){
				usleep(2500000);
				printf("tcpServerThread Poll.\n");
			}
    	}
    }
    else{
    	//Main System Loop
    	initThreads();
    	main_loop();

    }

}


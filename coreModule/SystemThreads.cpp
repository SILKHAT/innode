#include "SystemThreads.h"
#include "../librobotinno/SystemConfig.h"
#include "hardware/ch6dm_linux.h"					// IMU Operations
#include "hardware/adcAccess.h"						// ADC Operations
#include <netinet/in.h>

//Network Libraries
#include "../librobotinno/network/Package.h"
#include "../librobotinno/network/PackageConstants.h"
#include "../librobotinno/network/Client.h"

/*******************************************************************************
* Function Name  : adcThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (adcValues ...)
* Return         : None
* Description    : Reads adcDevice1 with an interval of adcSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/


int fd;

void *adcThreadRun(void *param)
{
	//Shared Memory Instance
	char *shm;
	float currentRead;

	int argc = 1;
	char **argv = NULL;

	int afd, i, ch = -1;
	int avg = 0, machine_read = 0, opt;

	char *adcTestMode;

	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

	adcTestMode = (char*) getSM(shm, SM::adcTest);	//Read corresponding variables of AdcThread

	afd = open(adcDevice, O_RDWR | O_NONBLOCK, 0);
	if (afd < 0) {
		perror("open");
		printf("[%s]:ADC Thread couldnt be opened\n", __func__);
		return NULL;
	}
	else{
		printf("[%s]:ADC Thread has opened ADCport\n", __func__);

	while( 1 == 1 ) {
		for (i = MIN_OVERO_CHANNEL; i <= MAX_OVERO_CHANNEL; i++){
			currentRead = read_channel(afd, i, avg, machine_read, shm);	//Read ADC port
			currentRead = currentRead + 1 - 1;							//Filter if appropriate

			if ( *adcTestMode ){
				(i == MIN_OVERO_CHANNEL) ? printf("ADC CH2: %f\n", currentRead) : currentRead;
			}
			switch( i){
				//Write to SharedMem
				case 0: writeToSM(shm, SM::QR::ADC1, currentRead); break;
				case 1: writeToSM(shm, SM::QR::ADC2, currentRead); break;
				case 2: writeToSM(shm, SM::QR::ADC3, currentRead); break;
				case 3: writeToSM(shm, SM::QR::ADC4, currentRead); break;
				case 4: writeToSM(shm, SM::QR::ADC5, currentRead); break;
				case 5: writeToSM(shm, SM::QR::ADC6, currentRead); break;
				case 6: writeToSM(shm, SM::QR::ADC7, currentRead); break;
				case 7: writeToSM(shm, SM::QR::ADC8, currentRead); break;
				default: break;
			}
		}
		usleep(adcSleepPeriod);
	}

	close(afd);
	}


}

/*******************************************************************************
* Function Name  : serial1ThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (imuRoll,imuPitch,imuYaw)
* Return         : None
* Description    : Reads serialDevice1 with an interval of serialSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *serial1ThreadRun(void *param)
{
	//Shared memory variables
	char *shm;
	//Thread specific variables
	float roll, pitch, yaw;				//Internal Storage
	float rollRate, pitchRate, yawRate; //Internal Storage
	float mx, my, mz; 				//3d magnetometer
	float ax, ay, az; 				//3d accelerometer
	float gx, gy, gz; 				//3d gyroscope


	char *serial1Test;					//Debugging

	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

	//Read corresponding variables of AdcThread
	serial1Test = (char*) getSM(shm, SM::serial1Test);

	//Serial Port Variables
	int fd;
	int numBytez;						//Number of Bytes read from serialInterface
	struct termios options;				//Define Serial Port Options

	if ( -1 == (fd = open_serial(serial1Device) )){	//Open Serial Port
		if ( *serial1Test ){
			printf("[%s]:Serial1 Thread cannot open port\n", __func__);
		}
		return NULL;
	}
	else{
		if ( *serial1Test )
			printf("[%s]:Serial1 Thread has opened port\n", __func__);
	}

	tcgetattr(fd, &options);			/*Configure Serial Port for Usage */
	cfsetispeed(&options, 115200);		/* Set the baud rates to 115200 */
	options.c_cflag |= (CLOCAL | CREAD);/* Enable the receiver and set local mode... */
	options.c_cflag |= CS8;				/* Select 8 data bits */

	options.c_cflag &= ~PARENB;			/* No parity (8N1) configuration */
	options.c_cflag &= ~CSTOPB;			/* No parity (8N1) configuration */
	options.c_cflag &= ~CSIZE;			/* No parity (8N1) configuration */
	options.c_cflag |= CS8;				/* No parity (8N1) configuration */
	tcsetattr(fd, TCSAFLUSH, &options); /* Apply changes immediately */

	unsigned char byte = 'o';
	unsigned char len;
	unsigned char dataBuffer[30];
	unsigned char flagByte1,flagByte2;

	//Chr6DM IMU Driver specifications. Read datasheet for further information
	while(1){
		search_header:
		while( byte != 's'){
			numBytez = read(fd, &byte, 1);
		}
		numBytez = read(fd, &byte, 1);
		if ( byte != 'n')
			goto search_header;
		numBytez = read(fd, &byte, 1);
		if ( byte != 'p')
			goto search_header;

		//Valid Package Start here
		if ( *serial1Test )
			printf("Valid Package Start Detected \n");

		numBytez = read(fd, &byte, 1);
		if ( byte != SENSOR_DATA){
			continue;
			/*Interpret ChannelData Here.
			 * v1.0 For now we Interpret Sensor Data Package
			 * v2.0 We will interpret more package types
			 */
		}

		numBytez = read(fd, &len, 1);
		numBytez = read(fd, &flagByte1, 1);
		numBytez = read(fd, &flagByte2, 1);
		/*Interpret ChannelData Here.
		 * v1.0 For now we get raw, pitch, roll with default settings
		 * v2.0 determine active channels and interpret all data
		 */

		if (*serial1Test) {
			printf("Type: %d\n", (unsigned int)byte);
			printf("Len: %d\n", (unsigned int)len);
			printf("Flag1: %d\n", (unsigned int)flagByte1);
			printf("Flag2: %d\n", (unsigned int)flagByte2);
		}

		/*Read all data to dataBuffer */
		int byteNum, readBytes = 0, requiredBytes = 30;
		while ( readBytes < requiredBytes){
			if ( -1 != ( byteNum = read(fd, &dataBuffer[readBytes], requiredBytes - readBytes) )){
				readBytes += byteNum;
			}

		}

        int act_channels = (flagByte1 << 8) | (flagByte2);
        int i = 0;
        int m_recentData[3];

        // Yaw angle
        if ((act_channels & 0x8000) != 0)
        {
            m_recentData[0] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            yaw = (float)m_recentData[0] * (.0109863);
            i += 2;
        }
        // Pitch angle
        if ((act_channels & 0x4000) != 0)
        {
        	m_recentData[1] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            pitch = (float)m_recentData[1] * (.0109863);
            i += 2;
        }

        // Roll angle
        if ((act_channels & 0x2000) != 0)
        {
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            roll = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // Yawrate angle
        if ((act_channels & 0x8000) != 0){
            m_recentData[0] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            yawRate = (float)m_recentData[0] * (.0109863);
            i += 2;
        }
        // Pitchrate angle
        if ((act_channels & 0x4000) != 0){
        	m_recentData[1] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            pitchRate = (float)m_recentData[1] * (.0109863);
            i += 2;
        }

        // Rollrate angle
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            rollRate = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // magnetometerx
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            mx = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // magnetometery
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            my = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // magnetometerz
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            mz = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // gyrox
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            gx = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // gyroy
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            gy = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // gyroz
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            gz = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // accelerometerx
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            ax = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // accelerometery
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            ay = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

        // accelerometerz
        if ((act_channels & 0x2000) != 0){
            m_recentData[2] = (short)((dataBuffer[i] << 8) | dataBuffer[i + 1]);
            az = (float)m_recentData[2] * (.0109863);
            i += 2;
        }

		//Flush Serial Buffer or we will process all packages by order...
		tcflush(fd, TCIOFLUSH );

		/***************SHARED MEMORY ACCESS ****************/
		if ( *serial1Test ){
			printf("yaw: %.2f pitch:%.2f roll:%.2f \n", yaw, pitch, roll);
			printf("yawR: %.2f pitchR:%.2f rollR:%.2f \n", yawRate, pitchRate, rollRate);
			printf("ax: %.2f ay:%.2f az:%.2f \n", ax, ay, az);
			printf("gx: %.2f gy:%.2f gz:%.2f \n", gx, gy, gz);

		}

		writeToSM(shm, SM::QR::roll, roll);
		writeToSM(shm, SM::QR::pitch, pitch);
		writeToSM(shm, SM::QR::yaw, yaw);
		writeToSM(shm, SM::QR::rollRate, rollRate);
		writeToSM(shm, SM::QR::pitchRate, pitchRate);
		writeToSM(shm, SM::QR::yawRate, yawRate);


		/***************SHARED MEMORY ACCESS ****************/

		usleep(serialSleepPeriod * 1000);		// first arg is in ms so...
		//system("clear");						//UNCOMMENT AFTER

	}

}

/*******************************************************************************
* Function Name  : sysStatusTXThreadRun
* Input          :
* Output         : Transmits system status to remote computer (Slow Comm)
* Return         : None
* Description    : Transmits system status with an interval of sysStatusSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *sysShmTransmitThreadRun(void *param)
{
	int MAXSIZE = 1024;

	//Bind to shared mem
	char *shm;
	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

	//Shared Memory Bindings
	char *quadPidTest = (char*) getSM(shm, SM::QR::quadPidTest);
	char *systemFailure = (char*) getSM(shm, SM::systemFailure);
	char *platformType = (char*) getSM(shm, SM::platformType);
	char *sysFail = (char*) getSM(shm, SM::systemFailure);
	char *adcTest = (char*) getSM(shm, SM::adcTest);
	char *serial1Test = (char*) getSM(shm, SM::serial1Test);
	char *rfControlToggle = (char*) getSM(shm, SM::rfControlToggle);
	char *computerControlToggle = (char*) getSM(shm, SM::computerControlToggle);

	//States
	float *roll =  (float*) getSM(shm, SM::QR::roll);
	float *pitch =  (float*) getSM(shm, SM::QR::pitch);
	float *yaw =  (float*) getSM(shm, SM::QR::yaw);
	float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);
	float *pitchRate =  (float*) getSM(shm, SM::QR::pitchRate);
	float *yawRate =  (float*) getSM(shm, SM::QR::yawRate);
	float *xPos =  (float*) getSM(shm, SM::xPos);
	float *yPos =  (float*) getSM(shm, SM::yPos);
	float *zPos =  (float*) getSM(shm, SM::zPos);

	int *md1 = (int*) getSM(shm, SM::QR::motorDuty1);
	int *md2 = (int*) getSM(shm, SM::QR::motorDuty2);
	int *md3 = (int*) getSM(shm, SM::QR::motorDuty3);
	int *md4 = (int*) getSM(shm, SM::QR::motorDuty4);
	int *u1 = (int*) getSM(shm, SM::QR::U1);
	int *u2 = (int*) getSM(shm, SM::QR::U2);
	int *u3 = (int*) getSM(shm, SM::QR::U3);
	int *u4 = (int*) getSM(shm, SM::QR::U4);
	int *RF1 = (int*) getSM(shm, SM::RFDuty1);
	int *RF2 = (int*) getSM(shm, SM::RFDuty2);
	int *RF3 = (int*) getSM(shm, SM::RFDuty3);
	int *RF4 = (int*) getSM(shm, SM::RFDuty4);


	//TCP Networking Variables
    struct sockaddr_in server_info;
    struct hostent *he;
    int socket_fd,num;
    char buffer[MAXSIZE];
    char recvBuff[64];
    int readCode = -1;

    if ((he = gethostbyname("10.42.0.1"))==NULL) {
        fprintf(stderr, "Cannot get host name\n");
        exit(1);
    }

    startConnection:

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0))== -1) {
        fprintf(stderr, "Socket Failure!!\n");
        exit(1);
    }

    memset(&server_info, 0, sizeof(server_info));
    server_info.sin_family = AF_INET;
    server_info.sin_port = htons(3490);
    server_info.sin_addr = *((struct in_addr *)he->h_addr);

    readCode = -1;
    while( readCode == -1){
        readCode = connect(socket_fd, (struct sockaddr *)&server_info, sizeof(struct sockaddr));
        perror("connect");
        usleep(1000 * 500);
    }


    Package package;
    package.initialize(PCK_DATA_SHM);

    while(1) {

    	memset(buffer, 0, MAXSIZE);

	    //Create SHM package
	    //Put up Motor Signals
	    package.value[0] = ((char*)(RF1))[0];
	    package.value[1] = ((char*)(RF1))[1];
	    package.value[2] = ((char*)(RF1))[2];
	    package.value[3] = ((char*)(RF1))[3];
	    package.value[4] = ((char*)(RF2))[0];
	    package.value[5] = ((char*)(RF2))[1];
	    package.value[6] = ((char*)(RF2))[2];
	    package.value[7] = ((char*)(RF2))[3];
	    package.value[8] = ((char*)(RF3))[0];
	    package.value[9] = ((char*)(RF3))[1];
	    package.value[10] = ((char*)(RF3))[2];
	    package.value[11] = ((char*)(RF3))[3];
	    package.value[12] = ((char*)(RF4))[0];
	    package.value[13] = ((char*)(RF4))[1];
	    package.value[14] = ((char*)(RF4))[2];
	    package.value[15] = ((char*)(RF4))[3];
	    // Put up motor duties
	    package.value[16] = ((char*)(md1))[0];
	    package.value[17] = ((char*)(md1))[1];
	    package.value[18] = ((char*)(md1))[2];
	    package.value[19] = ((char*)(md1))[3];
	    package.value[20] = ((char*)(md2))[0];
	    package.value[21] = ((char*)(md2))[1];
	    package.value[22] = ((char*)(md2))[2];
	    package.value[23] = ((char*)(md2))[3];
	    package.value[24] = ((char*)(md3))[0];
	    package.value[25] = ((char*)(md3))[1];
	    package.value[26] = ((char*)(md3))[2];
	    package.value[27] = ((char*)(md3))[3];
	    package.value[28] = ((char*)(md4))[0];
	    package.value[29] = ((char*)(md4))[1];
	    package.value[30] = ((char*)(md4))[2];
	    package.value[31] = ((char*)(md4))[3];
	    // Flight angles
	    package.value[32] = ((char*)(roll))[0];
	    package.value[33] = ((char*)(roll))[1];
	    package.value[34] = ((char*)(roll))[2];
	    package.value[35] = ((char*)(roll))[3];
	    package.value[36] = ((char*)(pitch))[0];
	    package.value[37] = ((char*)(pitch))[1];
	    package.value[38] = ((char*)(pitch))[2];
	    package.value[39] = ((char*)(pitch))[3];
	    package.value[40] = ((char*)(yaw))[0];
	    package.value[41] = ((char*)(yaw))[1];
	    package.value[42] = ((char*)(yaw))[2];
	    package.value[43] = ((char*)(yaw))[3];
	    // Virtual Controls
	    package.value[44] = ((char*)(u1))[0];
	    package.value[45] = ((char*)(u1))[1];
	    package.value[46] = ((char*)(u1))[2];
	    package.value[47] = ((char*)(u1))[3];
	    package.value[48] = ((char*)(u2))[0];
	    package.value[49] = ((char*)(u2))[1];
	    package.value[50] = ((char*)(u2))[2];
	    package.value[51] = ((char*)(u2))[3];
	    package.value[52] = ((char*)(u3))[0];
	    package.value[53] = ((char*)(u3))[1];
	    package.value[54] = ((char*)(u3))[2];
	    package.value[55] = ((char*)(u3))[3];
	    package.value[56] = ((char*)(u4))[0];
	    package.value[57] = ((char*)(u4))[1];
	    package.value[58] = ((char*)(u4))[2];
	    package.value[59] = ((char*)(u4))[3];
        //FillData

    	usleep(1000 * 50);
    	//*RF1 = *RF1 + 1;

    	package.length = sizeof(package);
	    package.serialize(buffer);


        //fgets(buffer,MAXSIZE-1,stdin);
	    printf("BufferLength: %d \n", int(sizeof(package)));


        if ((send(socket_fd,buffer, sizeof(package) , 0) == -1)) {
                fprintf(stderr, "Failure Sending Message\n");
                close(socket_fd);
                exit(1);
        }
        else {

  				printf("Client Current Message:\n");

                printf("[%d]", buffer[0]);
                short *ln = (short *) &buffer[1];
                printf("[%d]", *ln);
                for(int i = 3; i < 6; i++) { printf("[%d]", buffer[i]); }
                printf("\n");

                num = recv(socket_fd, recvBuff, sizeof(recvBuff),0);
                if ( num <= 0 )
                {
                        printf("Either Connection Closed or Error\n");
                        //Break from the While
                        goto startConnection;
                }

                //printf("Client:Message Received From Server -  %s\n",recvBuff);
           }
    }
    close(socket_fd);




}

void *tcpServerThreadRun(void *param)
{

	printf("[%s]:TCP server started\n",__func__);

	//Bind to shared mem
	char *shm;
	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

    //Shared Memory Snappings
    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);
    float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);

    //TCP Networking
    struct sockaddr_in server;
    struct sockaddr_in dest;
    int status,socket_fd, client_fd,num;
    socklen_t size;

    char buffer[1024];
    char gotMes[64] = {'O','K'};
    char *buff;
    int yes =1;

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0))== -1) {
        fprintf(stderr, "Socket failure!!\n");
        exit(1);
    }

    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }
    memset(&server, 0, sizeof(server));
    memset(&dest,0,sizeof(dest));
    server.sin_family = AF_INET;
    server.sin_port = htons(3495);
    server.sin_addr.s_addr = INADDR_ANY;
    if ((bind(socket_fd, (struct sockaddr *)&server, sizeof(struct sockaddr )))== -1)    { //sizeof(struct sockaddr)
        fprintf(stderr, "Binding Failure\n");
        exit(1);
    }

    if ((listen(socket_fd, 10))== -1){
        fprintf(stderr, "Listening Failure\n");
        exit(1);
    }


    Package package;

    while(1) {
anotherclient:
        size = sizeof(struct sockaddr_in);

        if ((client_fd = accept(socket_fd, (struct sockaddr *)&dest, &size))==-1 ) {
            perror("accept");
            exit(1);
        }
        printf("Server got connection from client %s\n", inet_ntoa(dest.sin_addr));

        int js1, js2, js3, js4;
        char cmd;

        while(1) {
                if ((num = recv(client_fd, buffer, sizeof(package),0))== -1) {
                        perror("recv");
                        goto anotherclient;
                }
                else if (num == 0) {
                        printf("Connection closed\n");
                        //So I can now wait for another client
                        break;
                }

                printf("num %d \n", num);
                buffer[num] = '\0';

                printf("package : %d \n", buffer[0]);
                printf("[%d]", buffer[0]);
                short *ln = (short *) &buffer[1];
                printf("[%d]", *ln);
                for(int i = 3; i < 6; i++) { printf("[%d]", buffer[i]); }
                printf("\n");

                //parse package
                package.deserialize(buffer);

            	printf("packlen: %d \n", package.length);

            	switch(package.tag){
					case PCK_DATA_JOYSTICK:
							//Copy content to SHM
							js1 = char_to_int( &package.value[0]);
							js2 = char_to_int(&package.value[4]);
							js3 = char_to_int(&package.value[8]);
							js4 = char_to_int(&package.value[12]);
							printf("Got joystick command. Will transfer it to TI.\n");
							//printf("js1 %d js2 %d js3 %d js4 %d \n", js1, js2, js3, js4);
						break;
					case PCK_COMMAND:
							cmd = package.value[0];
							if ( cmd == TI_REMOTE_ESC_INIT){
								printf("Got ESC init command. Will transfer it to TI.\n");
								sendCmdPacket(&fd, TI_REMOTE_ESC_INIT);
							}
							else if ( cmd == TI_REMOTE_ALLOW_FLIGHT){
								printf("Got flight allow command. Will transfer it to TI.\n");
								sendCmdPacket(&fd, TI_REMOTE_ALLOW_FLIGHT);
							}
							else if ( cmd == TI_REMOTE_FORBID_FLIGHT){
								printf("Got flight allow command. Will transfer it to TI.\n");
								sendCmdPacket(&fd, TI_REMOTE_FORBID_FLIGHT);
							}
							else if ( cmd == TI_REMOTE_IMU_BIAS_SET){
								printf("Got flight allow command. Will transfer it to TI.\n");
								sendCmdPacket(&fd, TI_REMOTE_IMU_BIAS_SET);
							}
						break;
            	}

                if ((send(client_fd,gotMes, sizeof(gotMes),0))== -1)
                {
                     fprintf(stderr, "Failure Sending Message\n");
                     close(client_fd);
                     break;
                }


        } //End of Inner While...
        //Close Connection Socket
        close(client_fd);
    } //Outer While

    close(socket_fd);

	printf("[%s]:TCP server went down.\n",__func__);

}

/*******************************************************************************
* Function Name  : serial2ThreadRun
* Input          : SharedMemory Pointer
* Output         : Updates Shared Memory Variables (comm with microcontroller?)
* Return         : None
* Description    : Reads serialDevice1 with an interval of serialSleepPeriod
*				   defined in SystemConfig.h
*******************************************************************************/
void *serial2ThreadRun(void *param)
{
	//Open Serial
	fd = open_serial(serial2Device);

	//Shared Memory Instance
	char *shm;
	if ( -1 == bindSharedMem(&shm ) ){
		printf("[%s]:SharedMemoryBinding Failed", __func__);
	}

	struct termios options;


	if (fd < 0) {
		perror("open");
		printf("Serial2 Thread couldnt open port\n");

	}
	else{
		printf("Serial2 Thread has opened port\n");

		//Configure Serial
		tcgetattr(fd, &options);
		/* Set the new options for the port... */
		/* Set the baud rates to 115200 */
		cfsetispeed(&options, 115200);
		/* Enable the receiver and set local mode... */
		options.c_cflag |= (CLOCAL | CREAD);
		/* Select 8 data bits */
		options.c_cflag |= CS8;
		/* No parity (8N1) configuration */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;
		tcsetattr(fd, TCSAFLUSH, &options);

		unsigned char byte = 'o';
		unsigned char len;
		unsigned int numBytes;
		char dataBuffer[32];
		unsigned char flagByte1,flagByte2;
		unsigned short motorDuty[4];
		int pulseDuty[4];
		float roll,pitch, yaw, rawYaw, rawSonar;
		float rollRate, pitchRate, yawRate;
		float sonar, medianFiltered, desiredAltitude, medianDiff, medianInt = 0;
		short hoverMode, RFflightAllow, flightAllow;
		int i;
		float U[4];

		//Read testmode
		char *serial2Debug;
		serial2Debug = (char*) getSM(shm, SM::serial2Test);	//Read corresponding variables of AdcThread


		while(1){
			search_header:
			while( byte != 's'){
				numBytes = read(fd, &byte, 1);
			}
			numBytes = read(fd, &byte, 1);
			if ( byte != 'u')
				goto search_header;

			//Valid Package Start here{
			/*
			if ( DEBUG)
			printf("Valid Package Start Detected \n");
			 */

			numBytes = read(fd, &byte, 1);
			numBytes = read(fd, &len, 1);

			/*
	if ( DEBUG) {
			printf("Type: %d\n", (unsigned int)byte);
			printf("Len: %d\n", (unsigned int)len);
	}*/

			if ( byte != TI_SENSOR_DATA && byte != TI_PID_DATA && byte != TI_RATE_DATA && byte != TI_PULSE_DATA && byte != TI_SONAR_DATA && byte != TI_PILOT_DATA){
				if ( serial2Debug ){
					printf("Warning there is a package type which is not supported : %x !\n", byte);
				}
				goto search_header;
			}


			/*Read all data to dataBuffer */
			int byteNum, readBytes = 0, requiredBytes = len;
			while ( readBytes < requiredBytes){
				if ( -1 != ( byteNum = read(fd, &dataBuffer[readBytes], requiredBytes - readBytes) )){
					readBytes += byteNum;
				}

			}

			/*Interpret packages individually*/
			if ( byte == TI_SENSOR_DATA){
				/*SULink only gets IMU values for now*/
				roll = char_to_float( &dataBuffer[0]);
				pitch = char_to_float( &dataBuffer[4]);
				yaw = char_to_float( &dataBuffer[8]);

				/*//old version
	mem->setRoll(roll);
	mem->setPitch(pitch);
	mem->setYaw(yaw);*/

				writeToSM(shm, SM::QR::roll, roll);
				writeToSM(shm, SM::QR::pitch, pitch);
				writeToSM(shm, SM::QR::yaw, yaw);

				if ( serial2Debug ){
					printf("IMU DATA CAME\n");

				printf("roll:%.2f pitch:%.2f yaw: %.2f \n", roll, pitch, yaw);
				}
			}
			else if ( byte == TI_PID_DATA){
				/*
	for(i = 0; i < len; i++)
	printf("[%x] ", dataBuffer[i]);
				 */

				motorDuty[0] = char_to_short( &dataBuffer[0]);
				motorDuty[1] = char_to_short( &dataBuffer[2]);
				motorDuty[2] = char_to_short( &dataBuffer[4]);
				motorDuty[3] = char_to_short( &dataBuffer[6]);
				U[0] = char_to_float( &dataBuffer[8]);
				U[1] = char_to_float( &dataBuffer[12]);
				U[2] = char_to_float( &dataBuffer[16]);
				U[3] = char_to_float( &dataBuffer[20]);

				/*
				mem->MotorDuty[0] = motorDuty[0];
				mem->MotorDuty[1] = motorDuty[1];
				mem->MotorDuty[2] = motorDuty[2];
				mem->MotorDuty[3] = motorDuty[3];
				mem->U[0] = U[0];
				mem->U[1] = U[1];
				mem->U[2] = U[2];
				mem->U[3] = U[3];
				*/
				writeToSM(shm, SM::QR::motorDuty1, motorDuty[0]);
				writeToSM(shm, SM::QR::motorDuty2, motorDuty[1]);
				writeToSM(shm, SM::QR::motorDuty3, motorDuty[2]);
				writeToSM(shm, SM::QR::motorDuty4, motorDuty[3]);
				writeToSM(shm, SM::QR::U1, U[0]);
				writeToSM(shm, SM::QR::U2, U[1]);
				writeToSM(shm, SM::QR::U3, U[2]);
				writeToSM(shm, SM::QR::U4, U[3]);

				if ( serial2Debug ){
					//printf("PID DATA CAME\n");
					printf("MotorDuties: %d %d %d %d \n", motorDuty[0], motorDuty[1], motorDuty[2], motorDuty[3]);
					printf("Virtual U: %f %f %f %f \n", U[0], U[1], U[2], U[3]);
				}
			}
			else if ( byte == TI_PULSE_DATA ){
				pulseDuty[0] = char_to_uint( &dataBuffer[0]);
				pulseDuty[1] = char_to_uint( &dataBuffer[4]);
				pulseDuty[2] = char_to_uint( &dataBuffer[8]);

				/*
				mem->PulseDuty[0] = pulseDuty[0];
				mem->PulseDuty[1] = pulseDuty[1];
				mem->PulseDuty[2] = pulseDuty[2];
				*/
				writeToSM(shm, SM::RFDuty1, pulseDuty[0]);
				writeToSM(shm, SM::RFDuty2, pulseDuty[1]);
				writeToSM(shm, SM::RFDuty3, pulseDuty[2]);

				if ( serial2Debug ) {
					printf("PULSE PACKAGE CAME : %d %d %d %d\n", pulseDuty[0], pulseDuty[1], pulseDuty[2], pulseDuty[3]);
					int *rf1 = (int*) getSM(shm, SM::RFDuty1);
				}
			}
			else if ( byte == TI_RATE_DATA ){
				rollRate = char_to_float( &dataBuffer[0]);
				pitchRate = char_to_float( &dataBuffer[4]);
				yawRate = char_to_float( &dataBuffer[8] );

				/*
				mem->imuRollRate = rollRate;
				mem->imuPitchRate = pitchRate;
				mem->imuYawRate = yawRate;
				*/
				writeToSM(shm, SM::QR::rollRate, rollRate);
				writeToSM(shm, SM::QR::pitchRate, pitchRate);
				writeToSM(shm, SM::QR::yawRate, yawRate);

				if ( serial2Debug ){
					//printf("RATE PACKAGE CAME : %f %f %f \n", rollRate, pitchRate, yawRate);
				}
			}
			else if(byte == TI_SONAR_DATA){
				sonar = char_to_float( &dataBuffer[0]);
				medianFiltered = char_to_float( &dataBuffer[4]);
				desiredAltitude = char_to_float( &dataBuffer[8]);
				medianDiff = char_to_float( &dataBuffer[12]);
				medianInt = char_to_float( &dataBuffer[16]);

				/*
				mem->sonar = sonar;
				mem->medianFiltered = medianFiltered;
				mem->desiredAltitude = desiredAltitude;
				mem->medianDiff = medianDiff;
				mem->medianInt = medianInt;
				*/

				//TO BE IMPLEMENTED BY BCAN.

				if ( serial2Debug )printf("SONAR PACKAGE CAME : %f %f %f %f %f \n", sonar, medianFiltered, desiredAltitude, medianDiff, medianInt);
			}
			else if(byte == TI_PILOT_DATA){
				hoverMode = char_to_short( &dataBuffer[0]);
				RFflightAllow = char_to_short( &dataBuffer[2]);
				flightAllow = char_to_short( &dataBuffer[4]);
				rawSonar = char_to_float( &dataBuffer[6]);

				/*
				mem->hoverMode = hoverMode;
				mem->flightAllow = flightAllow;
				mem->RFflightAllow = RFflightAllow;
				mem->rawSonar = rawSonar;
				*/

				//TO BE IMPLEMENTED BY BCAN./

				if ( serial2Debug ){
					//printf("PILOT PACKAGE CAME : %d %d %d %f \n", mem->hoverMode, mem->RFflightAllow, mem->flightAllow, mem->rawSonar);
				}
			}
			else{
				//Other package types not supported for now
				continue;
			}

			//Flush Serial Buffer or we will process all packages by order...
			tcflush(fd, TCIOFLUSH );
			usleep(serialSleepPeriod * 1000);
			//system("clear");


		}

	}
}



void  *cameraThreadRun(void *param)
{

}

/*******************************************************************************
* Function Name  : loggerThreadRun
* Input          :
* Output         : Logs system status & Shared Memory to Blackbox
* Return         : None
* Description    :
*******************************************************************************/
void *loggerThreadRun(void *param)
{

}

/*******************************************************************************
* Function Name  : quadPidThreadRun
* Input          :
* Output         : Updated virtual controls
* Return         : None
* Description    : This function is responsible for maintaining quadrotor control.
* 				   Having one infinite loop which is called each controlSleepPeriod
* 				   defined in SystemConfig.h
*******************************************************************************/
void *quadPidThreadRun(void *param)
{
	//Shared memory variables
	char *shm;
    //Thread specific variables;
    char *quadPidTest;
    float PIDPeriod = 0.01;
    float yawOld, pitchOld, rollOld;
    float yawErr, pitchErr, rollErr;
    float yawErrIntegral, pitchErrIntegral, rollErrIntegral;
    float w_1, w_2, w_3, w_4;

    int Kp_pitch, Kd_pitch, Ki_pitch;
    int Kp_yaw, Kd_yaw, Ki_yaw;

	if ( -1 == bindSharedMem(&shm ) ){
		printf("SharedMemoryBinding Failed: serial1ThreadRun");
	}

    // Get pointers from shared memory
    float *roll =  (float*) getSM(shm, SM::QR::roll);
    float *pitch =  (float*) getSM(shm, SM::QR::pitch);
    float *yaw =  (float*) getSM(shm, SM::QR::yaw);
    float *rollRate =  (float*) getSM(shm, SM::QR::rollRate);
    float *pitchRate =  (float*) getSM(shm, SM::QR::pitchRate);
    float *yawRate =  (float*) getSM(shm, SM::QR::yawRate);
    float *U1 =  (float*) getSM(shm, SM::QR::U1);
    float *U2 =  (float*) getSM(shm, SM::QR::U2);
    float *U3 =  (float*) getSM(shm, SM::QR::U3);
    float *U4 =  (float*) getSM(shm, SM::QR::U4);
    int *mDuty1 =  (int*) getSM(shm, SM::QR::motorDuty1);
    int *mDuty2 =  (int*) getSM(shm, SM::QR::motorDuty2);
    int *mDuty3 =  (int*) getSM(shm, SM::QR::motorDuty3);
    int *mDuty4 =  (int*) getSM(shm, SM::QR::motorDuty4);
    float *Kp_roll =  (float*) getSM(shm, SM::QR::kpRoll);
    float *Kd_roll =  (float*) getSM(shm, SM::QR::kdRoll);
    float *Ki_roll =  (float*) getSM(shm, SM::QR::kiRoll);

	//Read corresponding variables of AdcThread
	quadPidTest = (char*) getSM(shm, SM::QR::quadPidTest);

	while(1 == 1){

        //IMU Variables
        yawErr = (*yaw - yawOld) / PIDPeriod; //Yaw error = 0;
        pitchErr = (*pitch - pitchOld) / PIDPeriod;
        rollErr = (*roll - rollOld) / PIDPeriod;

        yawOld = *yaw;
        pitchOld = *pitch;
        rollOld = *roll;

        rollErrIntegral  = 0;
        pitchErrIntegral = 0;
        yawErrIntegral   = 0;

         //mem->U[0] = 34 + (Thrust_Duty - Thrust_Duty_bias) * Thrust_Duty_Gain ;
		 //*U2 = (Kp_roll * rollErr)   - (Kd_roll * (*rollRate)  ) + ( (*Ki_roll) * rollErrIntegral);
		 //*U3 = (Kp_pitch * pitchErr) - (Kd_pitch * (*pitchRate) ) + ( (*Ki_pitch) * pitchErrIntegral);
		 //*U4 = (Kp_yaw * yawErr)     - (Kd_yaw * (*yawRate) )    + ( (*Ki_yaw) * yawErrIntegral);
		 if (*quadPidTest ){
			 printf("[%s]: U1 %f U2 %f U3 %f U4 %f \n", __func__, *U1,*U2,*U3,*U4);//testdebug
		 }
		 //U1 Boundaries should be added

		 w_1 = *U1 - *U3 + *U4;
		 w_2 = *U1 + *U2 - *U4;
		 w_3 = *U1 + *U3 + *U4;
		 w_4 = *U1 - *U2 - *U4;

		 //ax+b w1 to rpm2 conversion should be added

		 //Motor Duty Calculation
		 //These functions has to be modeled according to the motor tests results

		 /*
		 *mDuty1 = 18000;
		 *mDuty2 = 18000;
		 *mDuty3 = 18000;
		 *mDuty4 = 18000;
		 */

		 //motor duty saturation limits should be added

		 //Update PWM
		 usleep(controlSleepPeriod * 1000);
	}


}


/*******************************************************************************
* Function Name  : quadMonitorThreadRun
* Input          :
* Output         : Modified logic checks in shared memory
* Return         : None
* Description    : This function is responsible for maintaining quadrotor states.
* 				   It periodically checks sensor readings, performs logical checks
* 				   and updates system states in the shared memory. It also has
* 				   privilege to stop the system in case of emergencies.
*******************************************************************************/
void *quadMonitorThreadRun(void *param)
{

}

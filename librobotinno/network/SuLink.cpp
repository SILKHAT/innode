#include "SuLink.h"

void open_serial(int *fd, const char *serialDevice)
{
	*fd = open(serialDevice, O_RDWR | O_NDELAY);
	if (*fd == -1){
	/*
	* Could not open the port.
	*/
	perror("open_port: Unable to open /dev/ttyS0 - ");
	}
	else
	fcntl(*fd, F_SETFL, 0);
}

void writeRetry(int *fd, char *buffer, int len)
{
	/*Write all data to dataBuffer */
	cout << " sending package " << endl;
	int j = 0;
	for(j = 0; j < 16; j++){
		printf("[%d] ", buffer[j] );
	}
	printf("\n");

	int byteNum, writtenBytes = 0, requiredBytes = len;
	while ( writtenBytes < requiredBytes){
		if ( -1 != ( byteNum = write(*fd, &buffer[writtenBytes], requiredBytes - writtenBytes) )){
			writtenBytes += byteNum;
		}
	}
}

void sendCmdPacket(int *fd, int packageType)
{
	int i;
	char buffer[16];
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = packageType;
	for(i = 3; i < 16; i++){
		buffer[i] = 0;
	}
	writeRetry(fd, buffer, 16);
}

void sendDataPacket(int *fd, int packageType, int len, char *data)
{
	int i;
	char buffer[16];
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = packageType;
	buffer[3] = len;
	for(i = 4; i < (len - 4); i++){
		buffer[i] = data[i - 4];
	}
	writeRetry(fd, buffer, 16);

}

void setPIDGains(int *fd, int type, float Kp, float Kd, float Ki)
{
	int i;
	char buffer[16];
	char tmp;
	printf("kp %f kd %f ki %f \n", Kp, Kd, Ki );
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = type;
	buffer[3] = 12;	//Transmission of 3 float takes 12 bytes

	memcpy(&buffer[4], &Kp, sizeof (Kp));
	memcpy(&buffer[8], &Kd, sizeof (Kd));
	memcpy(&buffer[12], &Ki, sizeof (Ki));

	writeRetry(fd, buffer, 16);

}

void setSonarGains(int *fd, int type, float Kp, float Kd, float Ki)
{
	int i;
	char buffer[16];
	char tmp;
	printf("sonarkp %f sonarkd %f sonarki %f \n", Kp, Kd, Ki);
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = type;
	buffer[3] = 12;	//Transmission of 2 float takes 8 bytes

	memcpy(&buffer[4], &Kp, sizeof (Kp));
	memcpy(&buffer[8], &Kd, sizeof (Kd));
	memcpy(&buffer[12], &Ki, sizeof (Ki));

	writeRetry(fd, buffer, 16);

}


void setMotorGains(int *fd, int type, float gain1, float gain2)
{
	int i;
	char buffer[16];
	char tmp;
	printf("gain1 %f gain2 %f \n", gain1, gain2 );
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = type;
	buffer[3] = 8;	//Transmission of 2 float takes 8 bytes

	memcpy(&buffer[4], &gain1, sizeof (gain1));
	memcpy(&buffer[8], &gain2, sizeof (gain2));

	writeRetry(fd, buffer, 16);

}

void setCompFilterValue(int *fd, int type, float value)
{
	int i;
	char buffer[16];
	char tmp;
	printf("Value: %f \n", value );
	buffer[0] = 's';
	buffer[1] = 'u';
	buffer[2] = type;
	buffer[3] = 4;	//Transmission of 1 float takes 4 bytes

	memcpy(&buffer[4], &value, sizeof (value));

	writeRetry(fd, buffer, 16);
}


int mainSuLinkTest()
{
	int *fd;
	bool SERIAL2_DEBUG = false;

	struct termios options;

	//Open Serial
	open_serial(fd,"/dev/ttyUSB0");

	if (*fd < 0) {
		perror("open");
		printf("Serial2 Thread couldnt open port\n");

	}
	else{
		printf("Serial2 Thread has opened port\n");

		//Configure Serial
		tcgetattr(*fd, &options);
		/* Set the new options for the port... */
		/* Set the baud rates to 115200 */
		cfsetispeed(&options, 115200);
		/* Enable the receiver and set local mode... */
		options.c_cflag |= (CLOCAL | CREAD);
		/* Select 8 data bits */
		options.c_cflag |= CS8;
		/* No parity (8N1) configuration */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;
		tcsetattr(*fd, TCSAFLUSH, &options);

		int key, key2;
		float kp, kd, ki;
		float m1Gain, m2Gain, m3Gain, m4Gain;
		float compFilterBeta, compFilterGamma;

		printf("What u wanna do ?\n");
		printf("1. Init ESC\n");
		printf("2. Allow Flight System to Run\n");
		printf("3. Dİsable Flight System (Stop Motors)\n");
		printf("4. Adjust Gains(ROLL)\n");
		printf("5. Adjust Gains(PITCH)\n");
		printf("6. Adjust Gains(YAW)\n");
		printf("7. Adjust Gains(THRUST/SONAR)\n");
		printf("8. Setup IMU Bias as (0,0) \n");
		printf("9. Motor1-2 Gain Set \n");
		printf("10. Motor3-4 Gain Set \n");
		printf("11. Setup Compass Bias as (0) \n");
		printf("12. Setup Compass LPF Beta \n");
		printf("13. Setup Compass LPF Gamma \n");
		std::cin >> key;
		cout << "key is : " << key << endl;
		if ( key == 1 ){
			cout << "remote init sending : " << key << endl;
			sendCmdPacket(fd, TI_REMOTE_ESC_INIT);
		}
		else if ( key == 2 ){
			sendCmdPacket(fd, TI_REMOTE_ALLOW_FLIGHT);
		}
		else if ( key == 3 ){
			sendCmdPacket(fd, TI_REMOTE_FORBID_FLIGHT);
		}
		else if ( key ==  4 ){
			cout << " Give Kp_Roll : ";
			cin >> kp;
			cout << " Give Kd_Roll : ";
			cin >> kd;
			cout << " Give Ki_Roll : ";
			cin >> ki;
			setPIDGains(fd, TI_SET_PID_ROLL_GAIN, kp, kd, ki);
		}
		else if ( key ==  5 ){
			cout << " Give Kp_Pitch : ";
			cin >> kp;
			cout << " Give Kd_Pitch : ";
			cin >> kd;
			cout << " Give Ki_Pitch : ";
			cin >> ki;
			setPIDGains(fd, TI_SET_PID_PITCH_GAIN, kp, kd, ki);
		}
		else if ( key ==  6 ){
			cout << " Give Kp_Yaw : ";
			cin >> kp;
			cout << " Give Kd_Yaw : ";
			cin >> kd;
			cout << " Give Ki_Yaw : ";
			cin >> ki;
			setPIDGains(fd, TI_SET_PID_YAW_GAIN, kp, kd, ki);
		}
		else if ( key ==  7 ){
			cout << " Give sonarKp : ";
			cin >> kp;
			cout << " Give sonarKd : ";
			cin >> kd;
			cout << " Give sonarKi : ";
			cin >> ki;
			setSonarGains(fd, TI_SET_SONAR_GAIN, kp, kd, ki);
		}
	        else if ( key == 8 ){
			sendCmdPacket(fd, TI_REMOTE_IMU_BIAS_SET);
		}
		else if ( key == 9 ){
			cout << "Give Motor1 Gain : ";
			cin >> m1Gain;
			cout << "Give Motor2 Gain : ";
			cin >> m2Gain;
			setMotorGains(fd, TI_REMOTE_MOTOR12_GAIN_SET, m1Gain, m2Gain);
		}
		else if ( key == 10 ){
			cout << "Give Motor3 Gain : ";
			cin >> m3Gain;
			cout << "Give Motor4 Gain : ";
			cin >> m4Gain;
			setMotorGains(fd, TI_REMOTE_MOTOR34_GAIN_SET, m3Gain, m4Gain);
		}
		else if ( key == 11){
			sendCmdPacket(fd, TI_REMOTE_COMPASS_INIT);
		}
		else if ( key == 12){
			cout << "Give Compass Filter Beta : ";
			cin >> compFilterBeta;
			setCompFilterValue(fd, TI_REMOTE_COMPFILTER_SET_BETA, compFilterBeta);
		}
		else if ( key == 13){
			cout << "Give Compass Filter Gamma : ";
			cin >> compFilterGamma;
			setCompFilterValue(fd, TI_REMOTE_COMPFILTER_SET_GAMMA, compFilterGamma);
		}

		return 0;
	}

}

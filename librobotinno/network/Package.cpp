#include "Package.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Package::Package()
{
    //ctor
}

Package::~Package()
{
    //dtor
}

void Package::printPackage(){
	// Print Received Package Type
    switch(tag){
	case PCK_BEACON: {
 			printf(" PCK_BEACON ");
			break;
		     }
	case PCK_ACK:    {
			printf(" PCK_ACK ");
			break;
		     }
	case PCK_DATA_JOYSTICK:    {
			printf(" PCK_DATA_JOYSTICK ");
			break;
		      }
	case PCK_ERROR:    {
			printf(" PCK_ERROR ");
			break;
		      }
	case PCK_DATA_SHM:    {
			printf(" PCK_DATA_SHM ");
			break;
		      }
	default:      {
 			 perror("ERROR: You forgot to add new package type support. Please modify PackageConstants.h to add your new packaage type \n");
		         break;
		      }
     }

    printf(" Package Received \n");
    printf("[%d][%d]", tag, length);
	for (int j = 0; j < length - sizeof(char) - sizeof(short); j++) {
		printf("[%d]", value[j]);
	}
	printf("\n");
	// Print Package Information
}
int Package::serialize(char *dst) const
{
   int i = 0;

   memcpy(&dst[i], &tag, sizeof tag);
   i += sizeof tag;

   memcpy(&dst[i], &length, sizeof length);
   i += sizeof length;

   memcpy(&dst[i], &value, PCK_LEN - 2);
   i += (PCK_LEN - 2);

   dst[PCK_LEN] = '\0';

   return i;
}

int Package::deserialize(char *dst)
{
   int i = 0;

   memcpy(&tag, &dst[i], sizeof tag);	//Tag 1 byte
   i += sizeof tag;

   memcpy(&length, &dst[i], 2);			//Short 2 bytes
   i += 2;

   memcpy(&value, &dst[i], length);
   i += length;

   return i;
}

int Package::initialize(int modifier){
    switch(modifier){
	case PCK_BEACON: {
 			tag = PCK_BEACON;
			break;
		     }
	case PCK_ACK:    {
 			tag = PCK_ACK;
			break;
		     }
	case PCK_DATA_JOYSTICK:    {
 			tag = PCK_DATA_JOYSTICK;
			break;
		      }
	case PCK_ERROR:    {
 			tag = PCK_ERROR;
			break;
		      }
	case PCK_DATA_SHM:    {
 			tag = PCK_DATA_SHM;
			break;
		      }
	case PCK_COMMAND:    {
 			tag = PCK_COMMAND;
			break;
		      }
	default:      {
 			 perror("ERROR: You forgot to add new package type support. Please modify PackageConstants.h to add your new package type \n");
			     exit(0);
		         break;
		      }
     }

     //this->length= sizeof(this->tag) + sizeof(this->length);
     return 0;
}

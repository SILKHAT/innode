#ifndef CLIENT_H
#define CLIENT_H

#include "PackageConstants.h"
#include "Package.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>
#include <errno.h>
#include <arpa/inet.h>
class Client
{
private:
	int sockfd, portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char buffer[1025];
    int portNo;

public:
    Client();
    int Connect(char* hostName, int portNo);
    void Send(const Package &package);
    void error(const char *msg);
    virtual ~Client();

};

#endif // CLIENT_H
